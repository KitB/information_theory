from __future__ import division
import collections
import math
import itertools


class SingleDist(collections.Counter):
    def update(self, iterable=None, **kwds):
        super(SingleDist, self).update(iterable, **kwds)
        self._normalize()

    def _normalize(self):
        z = sum(self.itervalues())
        for key in self.iterkeys():
            self[key] /= z

    @property
    def entropy(self):
        return sum([p_x * math.log(1 / p_x, 2) for p_x in self.itervalues()])

    def p(self, key):
        return self[key]

    def headerize(self):
        for key in self.iterkeys():
            self[key] = math.ceil(2**8 * self[key]) / 2**8
        self._normalize()


class BiGramDist(SingleDist):
    def __init__(self, iterable=None):
        (it1, it2, it3) = itertools.tee(iterable, 3)
        it2.next()
        bigrams = itertools.izip(it1, it2)
        super(BiGramDist, self).__init__(bigrams)
        self._backoff = SingleDist(it3)

    @property
    def conditional_entropy(self):
        v1 = sum([p_xy * math.log(p_x / p_xy, 2)
                  for (x, p_x) in self._backoff.iteritems()
                  for ((xx, y), p_xy) in self.iteritems()
                  if xx == x])
        v2 = self.entropy - self._backoff.entropy
        assert abs(v1 - v2) < 0.00001, "%f != %f" % (v1, v2)
        return v1

    def headerize(self):
        super(BiGramDist, self).headerize()
        self._backoff.headerize()
