% Information Theory Assignment
% Kit Barnes - s0905642

# Part one
## Question one
This question was fairly easy to implement, using Python 2.7's Counter class as
a base and adding normalization and an entropy calculation step as per the
formulae.

Distribution[^1]:

+---+---------+---+---------+---+---------+
| ␣ | 0.167   | h | 0.0306  | s | 0.0587  |
+---+---------+---+---------+---+---------+
| a | 0.0686  | k | 0.00577 | r | 0.0531  |
+---+---------+---+---------+---+---------+
| c | 0.0255  | j | 0.00181 | u | 0.0205  |
+---+---------+---+---------+---+---------+
| b | 0.0149  | m | 0.0277  | t | 0.0767  |
+---+---------+---+---------+---+---------+
| e | 0.094   | l | 0.0363  | w | 0.0109  |
+---+---------+---+---------+---+---------+
| d | 0.0273  | o | 0.0597  | v | 0.00912 |
+---+---------+---+---------+---+---------+
| g | 0.0178  | n | 0.0569  | y | 0.0105  |
+---+---------+---+---------+---+---------+
| f | 0.0162  | q | 0.00244 | x | 0.00873 |
+---+---------+---+---------+---+---------+
| i | 0.071   | p | 0.0261  | z | 0.00225 |
+---+---------+---+---------+---+---------+

And the entropy for this distribution is 4.17 bits.

## Question two
A bigram distribution was computed by zipping the list of input characters with
the tail of the list of input characters and running those through the same
mechanism as was used for the single letter distribution. Along with this the
distribution of single letters was calculated once more.

The joint entropy -- 7.57 bits -- was calculated exactly as entropy was
calculated for the single letter distribution but replacing letters with
bigrams.

This joint entropy is less than twice the individual entropy because the
distributions are not independent; the probability $P(Y=y)\neq P(Y=y\|X=x)$; as
a result each $Y$ reveals less information as it follows an $X$.

The conditional entropy -- 3.40 bits -- was then calculated both by subtracting
the single entropy from the joint entropy and by the iterative(/sum) method.
These two values were compared for equality[^2] as a check of correctness then
either value was returned (as they were roughly equal).

## Question three[^5]
The encoder will compress each bit at the entropy of the distribution calculated
(4.17) and with 344026 characters this gives us
$\lceil 4.17 \times 344026 \rceil = 1433834$ bits.

We have previously proven that arithmetic coding will encode up to 2 bits on top
of optimal so lets add 2 bits to that so $1433836$ bits.

With the conditional distribution we make much the same calculation, except we
use the conditional entropy for all bits after the first:

$$\lceil H(X) + H(Y\|X)(N-1)+2 \rceil = \lceil 4.17 + 3.4 \times 344025 + 2 \rceil = 1171195$$

## Question four
Lets say we store the headers by simply storing each probability as a byte, for
the single distribution this will result in 27 bytes, for the conditional
distribution this will be 601 bytes.

If we perform the same calculations as in the previous questions with the new
distribution we get:

$$\lceil 4.23 \times 344026 \rceil = 1454185$$

$$\lceil 4.23 + 4.84 \times 344025 + 2 \rceil = 1665416$$


## Question five
I have left this question unanswered due to time constraints. My apologies.

# Part two
## Question six
For this and the next question a Packet class was produced with methods for
XOR-ing and displaying as a string.

Returned string:

> User: s5559183948

## Question seven
This algorithm is perhaps best explained in pseudocode:

~~~~{.python}
while decoded_packets != source_packets:
    for p in decoded_packets:
        for q in packets with p.source_number in q.sources:
            q = q ^ p
            q.sources = q.sources \ p.source_number  # \ being set difference
~~~~

such that when a packet is reduced to having only one source left it is
automatically moved into `decoded_packets` and assigned a `source_number`.

Returned string:

> Password: X!3baA1z

Packets used:

> `[1, 2, 3, 5, 6, 7, 9, 10, 11, 12, 14, 15, 16, 18, 19, 20, 21, 22]`

## Question eight
The code I created was a simple chaining of two codes[^3]:

First apply a generalised Hamming Code (as described on Wikipedia), then apply a
repetition code with 3 repetitions.

As this code's rate and effectiveness vary with the length of the input packet I
am going to presume that all input packets are one byte in length. The rate for
this code is then $R = {8 \over 36} = {2 \over 9}$.

For the given values of $f$ the bit error probability was calculated empirically as[^4]:

+------+-----------------+---------------+
|  p   | # bits flipped  | $P(bit flip)$ |
+======+=================+===============+
| 0.4  | 29569940        | 0.370         |
+------+-----------------+---------------+
| 0.1  | 813873          | 0.0102        |
+------+-----------------+---------------+
| 0.01 | 0               | 0             |
+------+-----------------+---------------+

Seeing as I could not feasibly compute a non-zero probability of bit-flip on the
most powerful computer I could hire I will have to attempt an analytical
solution.

\newpage

# Appendix A - Source code for part 1


## distribution.py

~~~~{.python}
from __future__ import division
import collections
import math
import itertools


class SingleDist(collections.Counter):
    def update(self, iterable=None, **kwds):
        super(SingleDist, self).update(iterable, **kwds)
        self._normalize()

    def _normalize(self):
        z = sum(self.itervalues())
        for key in self.iterkeys():
            self[key] /= z

    @property
    def entropy(self):
        return sum([p_x * math.log(1 / p_x, 2) for p_x in self.itervalues()])

    def p(self, key):
        return self[key]

    def headerize(self):
        for key in self.iterkeys():
            self[key] = math.ceil(2**8 * self[key]) / 2**8
        self._normalize()


class BiGramDist(SingleDist):
    def __init__(self, iterable=None):
        (it1, it2, it3) = itertools.tee(iterable, 3)
        it2.next()
        bigrams = itertools.izip(it1, it2)
        super(BiGramDist, self).__init__(bigrams)
        self._backoff = SingleDist(it3)

    @property
    def conditional_entropy(self):
        v1 = sum([p_xy * math.log(p_x / p_xy, 2)
                  for (x, p_x) in self._backoff.iteritems()
                  for ((xx, y), p_xy) in self.iteritems()
                  if xx == x])
        v2 = self.entropy - self._backoff.entropy
        assert abs(v1 - v2) < 0.00001, "%f != %f" % (v1, v2)
        return v1

    def headerize(self):
        super(BiGramDist, self).headerize()
        self._backoff.headerize()
~~~~

## q1.py

~~~~{.python}
#!/usr/bin/env python2.7
import sys
import distribution

if __name__ == '__main__':
    try:
        with open(sys.argv[1], 'r') as input_file:
            text = input_file.read()
    except IndexError:
        print "Usage: %s <input_file>" % sys.argv[0]
        exit(1)

    alphabet_only = filter(lambda c: c in "abcdefghijklmnopqrstuvwxyz ", text)
    dist = distribution.SingleDist(alphabet_only)
    print "Distribution:"
    print "============="
    for k, v in dist.iteritems():
        print "%s: %.3g" % (k, v)
    print "-------------"
    print
    e = dist.entropy
    print "Entropy: %.3g (%s)" % (e, e)
~~~~

## q2.py

~~~~{.python}
#!/usr/bin/env python2.7
import sys
import distribution

if __name__ == '__main__':
    try:
        with open(sys.argv[1], 'r') as input_file:
            text = input_file.read()
    except IndexError:
        print "Usage: %s <input_file>" % sys.argv[0]
        exit(1)

    alphabet_only = filter(lambda c: c in "abcdefghijklmnopqrstuvwxyz ", text)
    dist = distribution.BiGramDist(alphabet_only)
    print "Distribution:"
    print "============="
    for k, v in dist.iteritems():
        print "%s: %.3g" % (k, v)
    print "-------------"
    print
    e, ce = dist.entropy, dist.conditional_entropy
    print "Joint Entropy: %.3g (%s)" % (e, e)
    print "Conditional Entropy: %.3g (%s)" % (ce, ce)
~~~~

\newpage

# Appendix B - Source code for part 2

## lt.py

~~~~{.python}
""" Implements a LT code decoder.

    This is not a well designed piece of code; I had an idea for tracking
    instances of a class which I might end up using in my Masters project and
    decided to test it out.
"""
import collections


class Queryable(object):
    """ An inheritable class that keeps track of all instances of itself
        in a dictionary keyed on attributes and values of those attributes.

        The values in these dictionaries are stored as sets of the instances of
        this class.

        If the value is a set or frozenset then we store the members of the set
        as keys rather than the set itself because we'll rarely want to query
        on an exact set.

        This is almost certainly a bad idea but it's quite fun to mess with.
    """

    # Named "cloud" because I imagine each instance as a particle of dust in a
    # cloud of dust.
    cloud = collections.defaultdict(lambda: collections.defaultdict(set))

    def __setattr__(self, attr_name, value):
        old_value = getattr(self, attr_name, None)

        # Delete old references to this object from the cloud
        if old_value is not None:
            if isinstance(old_value, frozenset) or isinstance(old_value, set):
                for v in old_value:
                    self.cloud[attr_name][v].discard(self)
            else:
                self.cloud[attr_name][value].discard(self)

        # Add new references
        if isinstance(value, frozenset) or isinstance(value, set):
            for v in value:
                self.cloud[attr_name][v].add(self)
        else:
            self.cloud[attr_name][value].add(self)

        # Actually update the attribute
        super(Queryable, self).__setattr__(attr_name, value)


class Packet(Queryable):
    singles = set()
    single_packets = set()
    source_packet_numbers = set()

    def __init__(self, string_or_list, indices_list=None, sequence_number=None):
        # Set the string
        if isinstance(string_or_list, basestring):
            self.string = string_or_list
        else:
            try:
                self.string = ''.join(chr(a) for a in string_or_list)
            except TypeError:
                # Can pass an int
                self.string = chr(string_or_list)

        # Set the index list
        if indices_list is not None:
            self.indices = frozenset(indices_list)
            self.source_packet_numbers |= self.indices
            self.d = len(self.indices)
            self.sequence_number = sequence_number

    @property
    def d(self):
        return self._d

    @d.setter
    def d(self, new):
        self._d = new
        if new == 1:
            pair = (self.packet_number, self.string)
            if pair not in self.singles:
                self.singles.add((self.packet_number, self.string))
                self.single_packets.add(self)

    @property
    def packet_number(self):
        if len(self.indices) != 1:
            raise AttributeError('Packets that have not been '
                                 'fully decoded have no packet_number')
        return list(self.indices)[0]

    def __xor__(self, other):
        xored = [chr(ord(a) ^ ord(b))
                 for (a, b) in zip(self.string, other.string)]
        string = ''.join(xored)
        return self.__class__(string)

    def __ixor__(self, other):
        xored = [chr(ord(a) ^ ord(b))
                 for (a, b) in zip(self.string, other.string)]
        self.string = ''.join(xored)
        try:
            if other.indices < self.indices:
                self.indices = self.indices - other.indices
                self.d = len(self.indices)
        except AttributeError:
            pass
        return self

    def __str__(self):
        return self.string

    def __repr__(self):
        try:
            return ("<%s with string %s and indices %s>" %
                    (self.__class__.__name__, repr(self.string),
                     list(self.indices)))
        except AttributeError:
            return ("<%s with string %s>" %
                    (self.__class__.__name__, repr(self.string)))

    def __len__(self):
        return len(self.string)

    @classmethod
    def step_singles(c):
        for p in c.cloud['d'][1].copy():
            d = list(p.indices)[0]
            for op in (c.cloud['indices'][d] - c.cloud['d'][1]).copy():
                op ^= p

    @classmethod
    def decode(c):
        while {n for (n, s) in c.singles} != c.source_packet_numbers:
            c.step_singles()

        return ''.join(s for (n, s) in sorted(c.singles))
~~~~

## q7.py

~~~~{.python}
#!/usr/bin/env python2.7
import sys
import lt


if __name__ == '__main__':
    try:
        with open(sys.argv[1], 'r') as received:
            packet_strings = map(int, received.readlines())

        with open(sys.argv[2], 'r') as indices:
            indiceses = [map(int, line.split()) for line in indices]
    except IndexError:
        print "Usage: %s <received_packets_file> <packet_indices_file>" %\
            sys.argv[0]
        sys.exit(1)

    packets = [lt.Packet(s, i, n) for (n, (s, i)) in enumerate(zip(packet_strings, indiceses))]
    print lt.Packet.decode()
    print sorted([p.sequence_number for p in lt.Packet.single_packets])
~~~~

## code.py

~~~~{.python}
""" Again this is very poor code hacked together in a hurry. I would not want
to maintain this code.

As a matter of fact I would rather like to delete it and rewrite it in a
cleaner way but many deadlines loom.

Also I recommend against ever using the bitarray module from numpy. I would
have been better served writing my own such module.
"""
# -*- coding: utf-8 -*-
from __future__ import division
import bitarray as bt
import collections
import random
import math


class Packet(object):
    def __init__(self, string_or_list):
        self._b = bt.bitarray()
        # Set the string
        if isinstance(string_or_list, basestring):
            self._b.frombytes(string_or_list)
        elif isinstance(string_or_list, bt.bitarray):
            self._b.extend(string_or_list)
        else:
            try:
                self._b.frombytes(''.join(chr(a) for a in string_or_list))
            except TypeError:
                # Can pass an int
                self._b.frombytes(chr(string_or_list))

    def __iter__(self):
        return iter(self._b)

    def __xor__(self, other):
        xored = self._b ^ other._b
        return self.__class__(xored)

    def __str__(self):
        return self._b.tobytes()

    def __list__(self):
        return list(self._b)

    def __len__(self):
        return len(self._b)

    def __getitem__(self, key):
        return self._b[key]

    def __setitem__(self, key, value):
        self._b[key] = value

    def __delitem__(self, key):
        del self._b[key]

    def __reversed__(self):
        return reversed(self._b)

    def __eq__(self, other):
        return self._b == other._b

    def __neq__(self, other):
        return self._b != other._b

    def __repr__(self):
        try:
            return ("<%s with string %s and indices %s>" %
                    (self.__class__.__name__, repr(self.string),
                     list(self.indices)))
        except AttributeError:
            return ("<%s with string %s>" %
                    (self.__class__.__name__, repr(str(self))))


def repetition_encode(packet, n=3):
    repetition_lists = [[b] * n for b in packet]
    flattened = [a for b in repetition_lists for a in b]
    return packet.__class__(bt.bitarray(flattened))


def repetition_decode(packet, n=3):
    chunks = chunk_list(packet, 3)
    bits = map(decode_bits, chunks)
    return packet.__class__(bt.bitarray(bits))


def decode_bits(bits):
    return collections.Counter(bits).most_common(1)[0][0]


def chunk_list(l, chunk_size):
    return [l[n:n+chunk_size] for n in xrange(0, len(l), chunk_size)]


def hamming_encode(packet):
    binary_powers = [1]
    n = 1
    it = iter(packet)
    l = []
    while True:
        # Choose bit to insert
        if n == binary_powers[-1]:
            binary_powers.append(binary_powers[-1] * 2)
            b = True
        else:
            try:
                b = it.next()
            except StopIteration:
                break
        # Insert it
        l.append(b)

        # Now xor into the parity bits
        for p in binary_powers[:-1]:
            if (n & p) != 0:
                l[p - 1] ^= b
        n += 1
    return packet.__class__(bt.bitarray(l))


def hamming_decode(packet):
    binary_powers = [1]
    n = 1
    it = iter(packet)
    l = []
    received_parity = []
    while True:
        try:
            b = it.next()
        except StopIteration:
            break

        if n == binary_powers[-1]:
            binary_powers.append(binary_powers[-1] * 2)
            received_parity.append(b)
            b = True

        # Insert it
        l.append(b)

        # Now xor into the parity bits
        for p in binary_powers[:-1]:
            if (n & p) != 0:
                l[p - 1] ^= b
        n += 1

    differing_parity = []
    for p in binary_powers[:-1]:
        exponent = int(math.floor(math.log(p, 2)))
        if received_parity[exponent] != l[p - 1]:
            differing_parity.append(p)

    if differing_parity:
        flipped_bit = sum(differing_parity) - 1
        try:
            l[flipped_bit] = not l[flipped_bit]
        except IndexError:
            pass

    out = [c for (m, c) in enumerate(l, 1) if m not in binary_powers]
    return packet.__class__(bt.bitarray(out))


def encode(packet):
    return repetition_encode(hamming_encode(packet))


def decode(packet):
    return hamming_decode(repetition_decode(packet))


def send_packet(packet, f=0.01):
    return decode(noisy_channel_send(encode(packet), f))


def until_error(p, f=0.01):
    n = 0
    while True:
        try:
            n += 1
            pp = send_packet(p, f)
            if not (p == pp):
                return (n, p, pp)
        except KeyboardInterrupt:
            return (n, p, pp)


def count_errors(p, f=0.01, iterations=10000):
    error_count = 0
    for _ in xrange(iterations):
        pp = send_packet(p, f)
        if not (p == pp):
            error_count += 1
    return error_count


def noisy_channel_send(packet, f=0.01):
    def do_flip(bit):
        if random.random() > f:
            return bit
        else:
            return not bit
    bits = [do_flip(bit) for bit in packet]
    return packet.__class__(bt.bitarray(bits))
~~~~

[^1]: I Just wanted to put a table in
[^2]: In so far as one can check floating point values for equality
[^3]: I tried to do something more fun for you but again there are many
      deadlines
[^4]: By running the code on ten-million packets of one byte each. I had to hire
      a 24-CPU compute instance to compute this without getting bored. I'm sure
      I could have optimised my code a bit more.
[^5]: I have a distinct feeling this is wrong but I don't have time to do it
      right. Something involving $D_{KL}$ but I don't know the correct
      underlying distribution. Same goes for Q4.
