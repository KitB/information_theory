""" Again this is very poor code hacked together in a hurry. I would not want
to maintain this code.

As a matter of fact I would rather like to delete it and rewrite it in a
cleaner way but many deadlines loom.

Also I recommend against ever using the bitarray module from numpy. I would
have been better served writing my own such module.
"""
# -*- coding: utf-8 -*-
from __future__ import division
import bitarray as bt
import collections
import random
import math


class Packet(object):
    def __init__(self, string_or_list):
        self._b = bt.bitarray()
        # Set the string
        if isinstance(string_or_list, basestring):
            self._b.frombytes(string_or_list)
        elif isinstance(string_or_list, bt.bitarray):
            self._b.extend(string_or_list)
        else:
            try:
                self._b.frombytes(''.join(chr(a) for a in string_or_list))
            except TypeError:
                # Can pass an int
                self._b.frombytes(chr(string_or_list))

    def __iter__(self):
        return iter(self._b)

    def __xor__(self, other):
        xored = self._b ^ other._b
        return self.__class__(xored)

    def __str__(self):
        return self._b.tobytes()

    def __list__(self):
        return list(self._b)

    def __len__(self):
        return len(self._b)

    def __getitem__(self, key):
        return self._b[key]

    def __setitem__(self, key, value):
        self._b[key] = value

    def __delitem__(self, key):
        del self._b[key]

    def __reversed__(self):
        return reversed(self._b)

    def __eq__(self, other):
        return self._b == other._b

    def __neq__(self, other):
        return self._b != other._b

    def __repr__(self):
        try:
            return ("<%s with string %s and indices %s>" %
                    (self.__class__.__name__, repr(self.string),
                     list(self.indices)))
        except AttributeError:
            return ("<%s with string %s>" %
                    (self.__class__.__name__, repr(str(self))))


def repetition_encode(packet):
    repetition_lists = [[b, b, b] for b in packet]
    flattened = [a for b in repetition_lists for a in b]
    return packet.__class__(bt.bitarray(flattened))


def repetition_decode(packet):
    bits = []
    n = 0
    n_zero = 0
    n_one = 0
    for bit in packet:
        n += 1
        if bit:
            n_one += 1
        else:
            n_zero += 1

        if n == 3:
            n = 0
            if n_zero > n_one:
                bits.append(0)
            else:
                bits.append(1)
            n_zero = 0
            n_one = 0
    return packet.__class__(bt.bitarray(bits))


def decode_bits(bits):
    return collections.Counter(bits).most_common(1)[0][0]


def hamming_encode(packet):
    binary_powers = [1]
    n = 1
    it = iter(packet)
    l = []
    while True:
        # Choose bit to insert
        if n == binary_powers[-1]:
            binary_powers.append(binary_powers[-1] * 2)
            b = True
        else:
            try:
                b = it.next()
            except StopIteration:
                break
        # Insert it
        l.append(b)

        # Now xor into the parity bits
        for p in binary_powers[:-1]:
            if (n & p) != 0:
                l[p - 1] ^= b
        n += 1
    return packet.__class__(bt.bitarray(l))


def bin_list(s):
    return [s] if s <= 1 else bin_list(s >> 1) + [s & 1]


def hamming_decode(packet):
    cdef int n, nbp, p
    cdef int binary_powers[8]
    binary_powers[0] = 1
    nbp = 1
    n = 1
    it = iter(packet)
    l = []
    received_parity = []
    while True:
        try:
            b = it.next()
        except StopIteration:
            break

        if n == binary_powers[nbp - 1]:
            binary_powers[nbp] = binary_powers[nbp - 1] * 2
            nbp += 1
            received_parity.append(b)
            b = True

        # Insert it
        l.append(b)

        # Now xor into the parity bits
        for p in binary_powers[:-1]:
            if (n & p):
                l[p - 1] ^= b
        n += 1

    differing_parity = []
    for p in binary_powers[:-1]:
        exponent = int(math.floor(math.log(p, 2)))
        if received_parity[exponent] != l[p - 1]:
            differing_parity.append(p)

    if differing_parity:
        flipped_bit = sum(differing_parity) - 1
        try:
            l[flipped_bit] = not l[flipped_bit]
        except IndexError:
            pass

    out = [c for (m, c) in enumerate(l) if m + 1 not in binary_powers]
    return packet.__class__(bt.bitarray(out))


def encode(packet):
    return repetition_encode(hamming_encode(packet))


def decode(packet):
    return hamming_decode(repetition_decode(packet))


def send_packet(packet, f=0.01):
    return decode(noisy_channel_send(encode(packet), f))


def until_error(p, f=0.01):
    n = 0
    while True:
        try:
            n += 1
            pp = send_packet(p, f)
            if not (p == pp):
                return (n, p, pp)
        except KeyboardInterrupt:
            return (n, p, pp)


def count_errors(p, f=0.01, iterations=10000):
    error_count = 0
    bit_error_count = 0
    for _ in xrange(iterations):
        pp = send_packet(p, f)
        if not (p == pp):
            error_count += 1
            bit_error_count += len([1 for (b1, b2) in zip(p._b, pp._b)
                                    if b1 != b2])
    return error_count, bit_error_count


def noisy_channel_send(packet, f=0.01):
    def do_flip(bit):
        if random.random() > f:
            return bit
        else:
            return not bit
    bits = [do_flip(bit) for bit in packet]
    return packet.__class__(bt.bitarray(bits))
