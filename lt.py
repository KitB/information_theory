""" Implements a LT code decoder.

    This is not a well designed piece of code; I had an idea for tracking
    instances of a class which I might end up using in my Masters project and
    decided to test it out.
"""
import collections


class Queryable(object):
    """ An inheritable class that keeps track of all instances of itself
        in a dictionary keyed on attributes and values of those attributes.

        The values in these dictionaries are stored as sets of the instances of
        this class.

        If the value is a set or frozenset then we store the members of the set
        as keys rather than the set itself because we'll rarely want to query
        on an exact set.

        This is almost certainly a bad idea but it's quite fun to mess with.
    """

    # Named "cloud" because I imagine each instance as a particle of dust in a
    # cloud of dust.
    cloud = collections.defaultdict(lambda: collections.defaultdict(set))

    def __setattr__(self, attr_name, value):
        old_value = getattr(self, attr_name, None)

        # Delete old references to this object from the cloud
        if old_value is not None:
            if isinstance(old_value, frozenset) or isinstance(old_value, set):
                for v in old_value:
                    self.cloud[attr_name][v].discard(self)
            else:
                self.cloud[attr_name][value].discard(self)

        # Add new references
        if isinstance(value, frozenset) or isinstance(value, set):
            for v in value:
                self.cloud[attr_name][v].add(self)
        else:
            self.cloud[attr_name][value].add(self)

        # Actually update the attribute
        super(Queryable, self).__setattr__(attr_name, value)


class Packet(Queryable):
    singles = set()
    single_packets = set()
    source_packet_numbers = set()

    def __init__(self, string_or_list, indices_list=None, sequence_number=None):
        # Set the string
        if isinstance(string_or_list, basestring):
            self.string = string_or_list
        else:
            try:
                self.string = ''.join(chr(a) for a in string_or_list)
            except TypeError:
                # Can pass an int
                self.string = chr(string_or_list)

        # Set the index list
        if indices_list is not None:
            self.indices = frozenset(indices_list)
            self.source_packet_numbers |= self.indices
            self.d = len(self.indices)
            self.sequence_number = sequence_number

    @property
    def d(self):
        return self._d

    @d.setter
    def d(self, new):
        self._d = new
        if new == 1:
            pair = (self.packet_number, self.string)
            if pair not in self.singles:
                self.singles.add((self.packet_number, self.string))
                self.single_packets.add(self)

    @property
    def packet_number(self):
        if len(self.indices) != 1:
            raise AttributeError('Packets that have not been '
                                 'fully decoded have no packet_number')
        return list(self.indices)[0]

    def __xor__(self, other):
        xored = [chr(ord(a) ^ ord(b))
                 for (a, b) in zip(self.string, other.string)]
        string = ''.join(xored)
        return self.__class__(string)

    def __ixor__(self, other):
        xored = [chr(ord(a) ^ ord(b))
                 for (a, b) in zip(self.string, other.string)]
        self.string = ''.join(xored)
        try:
            if other.indices < self.indices:
                self.indices = self.indices - other.indices
                self.d = len(self.indices)
        except AttributeError:
            pass
        return self

    def __str__(self):
        return self.string

    def __repr__(self):
        try:
            return ("<%s with string %s and indices %s>" %
                    (self.__class__.__name__, repr(self.string),
                     list(self.indices)))
        except AttributeError:
            return ("<%s with string %s>" %
                    (self.__class__.__name__, repr(self.string)))

    def __len__(self):
        return len(self.string)

    @classmethod
    def step_singles(c):
        for p in c.cloud['d'][1].copy():
            d = list(p.indices)[0]
            for op in (c.cloud['indices'][d] - c.cloud['d'][1]).copy():
                op ^= p

    @classmethod
    def decode(c):
        while {n for (n, s) in c.singles} != c.source_packet_numbers:
            c.step_singles()

        return ''.join(s for (n, s) in sorted(c.singles))
